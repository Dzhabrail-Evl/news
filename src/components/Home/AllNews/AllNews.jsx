import Container from "../../Container/Container";
import CardNews from "../CardNews/CardNews";
import "./AllNews.scss";

export default function AllNews({ allNews }) {

    return (
        <section className="all-news">
            <Container>
                <div className="all-news__news">
                    {!allNews ? <h1>LOADING</h1> : ""}
                    {allNews?.map((post) => 
                        <div key={post.id}>
                            <CardNews post={post}/>
                        </div>
                    )}
                </div>
            </Container>
        </section>
    );
};