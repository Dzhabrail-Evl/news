import Header from "../Layout/Header/Header";
import Home from "../pages/Home/Home";
import InfoNews from "../components/InfoNews/InfoNews";
import NoteFoundPage from "../pages/NoteFoundPage/NoteFoundPage";

export {Header, Home, InfoNews, NoteFoundPage}