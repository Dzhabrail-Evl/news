import "./Input.scss";

export default function Input(
    {
        className="input__primary",
        width="300px",
        margin="0 0 0 0",
        label,
        placeholder="text",
        name="input",
        type="text",
        required=false,
        value,
        onChange=() => {},
        onBlur=() => {},
        check
    },
    ...props) {

    let error_mewssage = "";
    check?.length < 5 ? error_mewssage="field must not contain less than 5 characters" : error_mewssage="";
    
    return (
        <div 
            className={`
                input 
                ${className}
                ${check?.length < 5 ? "input__error" : "input__not-error"}
            `}
            style={{margin: margin, width: width}}
        >
            <label className="input__label">{label}</label>
            <input 
                placeholder={placeholder}
                name={name}
                type={type}
                required={required}
                value={value}
                onChange={onChange}
                onBlur={onBlur}
                {...props}
            />
            <span>{error_mewssage}</span>
        </div>
    );
};