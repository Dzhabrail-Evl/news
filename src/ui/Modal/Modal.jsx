import { Icon } from "..";
import "./Modal.scss";

export default function Modal({children, setIsOpen, isOpen}) {
    return (
        <div className={`modal ${!isOpen ? "modal--hidden" : ""}`}>
            <div className="modal__content">
                <div onClick={() => setIsOpen(false)} className="modal__close-modal">
                    <Icon name="close_modal"/>
                </div>
                {children}
            </div>
        </div>
    );
};