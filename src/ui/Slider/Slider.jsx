import { ArrowsSlider, NavigationCircles } from "../../components/Slider";
import "./Slider.scss";

export default function Slider({children, length=0, setSliderFlipping, sliderFlipping, width="500px", height="300px"}) {
    return (
        <div className="slider">
            <div className="slider__content">
                {length > 0 ? 
                    <ArrowsSlider length={length} setSliderFlipping={setSliderFlipping} sliderFlipping={sliderFlipping}>
                        <div 
                            className="slider__window"
                            style={{width: width, height: height}}
                        >
                            {children}
                        </div>
                    </ArrowsSlider>
                : 
                    <div 
                        className="slider__window" 
                        style={{width: width, height: height}}
                    >
                        {children}
                    </div>
                }
            </div>
            <div className="slider__content">
                <NavigationCircles length={length} sliderFlipping={sliderFlipping} setSliderFlipping={setSliderFlipping}/>
            </div>
        </div>
    );
};