import axios from "axios";

const Api = axios.create({
    baseURL: "http://localhost:3000/"
})

let token = localStorage.getItem("token")

Api.interceptors.response.use(
    (response) => {
        const {data} = response
        if(data.accessToken) {
            localStorage.setItem("token", data.accessToken)
            localStorage.setItem("user", JSON.stringify(data.user))
        }
        return data;
    },
    (error) => Promise.reject(error)
)

Api.interceptors.request.use(
    (request) => {
        if(!token) {
            token = localStorage.getItem("token")
        }
        if(token) {
            request.headers["Authorization"] = `Bearer ${token}`
        }
        return request;
    },
    (error) => Promise.reject(error)
)

export default Api;