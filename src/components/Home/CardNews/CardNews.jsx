import { useContext } from "react";
import { Link } from "react-router-dom";
import { newsApi } from "../../../api/newsApi";
import { Button, Icon } from "../../../ui";
import { NewsContext } from "../../../pages/Home/Home";
import "./CardNews.scss";

export default function CardNews({ post }) {

    let user;
    localStorage.getItem("user") ? user = "admin" : user = "";

    const setAllNews = useContext(NewsContext)

    const addCurrentNews = (id) => {
        newsApi.changePostActual(id, true).then(res => {
            setAllNews(
                prev => prev.map(p => {
                    if (p.id === id){
                        p.actual = true;
                    }
                    return p;
                })
            )
        });
    };

    const deleteNews = (id) => {
        newsApi.deletePost(id, true).then(res => {
            window.location.reload();
        });
    };

    return (
        <div className="card">
            {user ? 
                <div>
                    <div className="card__unfavourites" onClick={() => deleteNews(post.id)}>
                        <Icon name="add_post_actual"/>
                    </div>
                    <div className="card__favourites" onClick={() => addCurrentNews(post.id)}>
                        <Icon name="add_post_actual"/>
                    </div>
                </div>
            : ""}
            <h2 className="card__title">{post.title}</h2>
            <div className="card__img">
                <img src={post.src} alt="News logo" />
            </div>
            <p className="card__text">{post.text}</p>
            <div className="card__btn">
                <Link to={`posts/${post.id}`}>
                    <Button text="Подробнее" margin="20px auto 0 auto"/>
                </Link>
            </div>
        </div>
    );
};