import Modal from "./Modal/Modal";
import Icon from "./Icon/Icon";
import Slider from "./Slider/Slider";
import Input from "./Input/Input";
import Button from "./Button/Button";

export {Modal, Icon, Slider, Input, Button};