import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { newsApi } from "../../api/newsApi";
import { Icon } from "../../ui";
import RedactorInput from "../../ui/RedactorInput/RedactorInput";
import Container from "../Container/Container";
import "./InfoNews.scss";

export default function InfoNews() {

    const user = localStorage.getItem("user");
    const [newsMoreInfo, setNewsMoreInfo] = useState();
    const navigate = useNavigate();
    const {id} = useParams();

    const goBack = () => navigate(-1);
    
    // --------|| РЕДАКТОР ДЛЯ TITLE ||-------- //
    const [edit, setEdit] = useState(false);
    const startEditTitleSwitch = () => {
        setEdit((prev) => !prev);
    }; // если edit === true то мы можем редактировать поле
    const endEditTitleSwitch = (event) => {
        setEdit((prev) => !prev);
        newsApi.changePostTitle(id, event.target.value).then((res) => {
            setNewsMoreInfo(res);
        });
    }; // функция отработает когда снимаем фокус эффект с инпута, и отправляет patch запрос с новым текстом
    const handleTitleEdit = (value) => {
        setNewsMoreInfo((prev) => ({
            ...prev,
            title: value
        }));
    }; // функция вызывается когда мы меняем текст в инпуте, и берет поле title у newsMoreInfo и записывает туда новое value которе получаем из инпута
    
    
    //-------------------------------------------------------
    
    
    // --------|| РЕДАКТОР ДЛЯ IMG ||-------- //
    const [editImg, setEditImg] = useState();
    const startEditImgSwitch = () => {
        setEditImg((prev) => !prev);
    };
    const endEditImgSwitch = (event) => {
        setEditImg((prev) => !prev);
        newsApi.changePostImg(id, event.target.value).then((res) => {
            setNewsMoreInfo(res);
        });
    };
    const handleImgEdit = (value) => {
        setNewsMoreInfo((prev) => ({
            ...prev,
            src: value
        }));
    }
    
    
    //-------------------------------------------------------


    // --------|| РЕДАКТОР ДЛЯ SUBTITLE ||-------- //
    const [editSubtitle, setEditSubtitle] = useState(false);
    const startEditSubtitleSwitch = () => {
        setEditSubtitle((prev) => !prev);
    };
    const endEditSubtitleSwitch = (event) => {
        setEditSubtitle((prev) => !prev);
        newsApi.changePostSubtitle(id, event.target.value).then((res) => {
            setNewsMoreInfo(res);
        });
    };
    const handleSubtitleEdit = (value) => {
        setNewsMoreInfo((prev) => ({
            ...prev,
            subtitle: value
        }));
    };
    
    
    //-------------------------------------------------------


    // --------|| РЕДАКТОР ДЛЯ TEXT ||-------- //
    const [editText, setEditText] = useState(false);
    const startEditTextSwitch = () => {
        setEditText((prev) => !prev);
    };
    const endEditTextSwitch = (event) => {
        setEditText((prev) => !prev);
        newsApi.changePostText(id, event.target.value).then((res) => {
            setNewsMoreInfo(res);
        });
    };
    const handleTextEdit = (value) => {
        setNewsMoreInfo((prev) => ({
            ...prev,
            text: value
        }));
    };


    useEffect(() => {
        newsApi.getIdNews(id).then((res) => {
            setNewsMoreInfo(res);
        });
    },[]);

    return (
        <div className="info-news">
            <Container>
                <div className="info-news__back" onClick={goBack}>
                    <Icon name="go_back"/>
                </div>
                {editImg && user ? 
                    <RedactorInput 
                        value={newsMoreInfo?.src}
                        onChange={(event) => handleImgEdit(event.target.value)}
                        onBlur={endEditImgSwitch}
                        size="35px"
                        weight={600}
                        position="center"
                        margin="25px 0 0 0"
                    />
                : 
                    <div className="info-news__img">
                        <img src={newsMoreInfo?.src} alt="" onClick={startEditImgSwitch}/>
                    </div>
                }
                {edit && user ? 
                    <RedactorInput 
                        value={newsMoreInfo?.title}
                        onChange={(event) => handleTitleEdit(event.target.value)}
                        onBlur={endEditTitleSwitch}
                        size="35px"
                        weight={600}
                        position="center"
                        margin="25px 0 0 0"
                    />
                : 
                    <h2 className="info-news__title" onClick={startEditTitleSwitch}>{newsMoreInfo?.title}</h2>
                }
                {editSubtitle && user ? 
                    <RedactorInput 
                        value={newsMoreInfo?.subtitle}
                        onChange={(event) => handleSubtitleEdit(event.target.value)}
                        onBlur={endEditSubtitleSwitch}
                        weight="bold"
                        margin="25px 0 0 0"
                    />
                : 
                    <p className="info-news__subtitle" onClick={startEditSubtitleSwitch}>{newsMoreInfo?.subtitle}</p>
                }
                {editText && user ? 
                    <RedactorInput 
                        value={newsMoreInfo?.text}
                        onChange={(event) => handleTextEdit(event.target.value)}
                        onBlur={endEditTextSwitch}
                        size="16px"
                        margin="25px 0 0 0"
                    />
                : 
                    <p className="info-news__text" onClick={startEditTextSwitch}>{newsMoreInfo?.text}</p>
                }
                {newsMoreInfo?.gallery?.map((i) => 
                    <div key={i.id} className="info-news__gallery">
                        <img src={i.src} alt="Image" />
                    </div>
                )}
                {newsMoreInfo?.texts?.map((i) => 
                    <div key={i.id} className="info-news__more-text">
                        <p>{i.text}</p>
                    </div>
                )}
            </Container>
        </div>
    );
};