import "./Button.scss";

export default function Button({
    className="button__primary", 
    onClick=() => {},
    width="200px", 
    height="45px", 
    margin="0", 
    type="submit", 
    text="",
    disabled=false,
    ...props
}) {
    return (
        <div className="button">
            <button
                className={className}
                onClick={onClick}
                style={{
                    width: width,
                    height: height,
                    margin: margin
                }}
                type={type}
                disabled={disabled}
                {...props}
            >
                {text}
            </button>
        </div>
    );
};