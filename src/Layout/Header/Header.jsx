import Container from "../../components/Container/Container";
import Auth from "../../components/Auth/Auth";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import "./Header.scss";

export default function Header({children}) {


    return (
        <Fragment>
            <header className="header">
                <Container>
                    <div className="header__content">
                        <div className="header__logo">
                            <Link to="/">
                                <img src="/img/logo.png" alt="logo"/>
                            </Link>
                        </div>
                        <Auth />
                    </div>
                </Container>
            </header>
            {children}
        </Fragment>
    );
};