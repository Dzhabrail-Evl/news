import "./RedactorInput.scss";

export default function RedactorInput({value, onChange, onBlur, size="20px", weight="normal", position="left", margin="0"}) {
    return (
        <div className="redactor-input">
            <input 
                type="text"
                value={value}
                onChange={onChange}
                onBlur={onBlur}
                autoFocus
                wrap="sofr"

                style={{
                    fontSize: size,
                    fontWeight: weight,
                    textAlign: position,
                    margin: margin,
                }}
            />
        </div>
    );
};