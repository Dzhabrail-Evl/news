import React from "react";
import icons from "./icons.json";
import "./Icon.scss";

export default function Icon({onClick, name, className}) {
    return (
        <div
            className={`icon ${className}`}
            onClick={onClick}
            dangerouslySetInnerHTML={{__html: icons[name]}}
        >
        </div>
    );
};