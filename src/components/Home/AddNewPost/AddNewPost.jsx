import { Formik } from "formik";
import { useState } from "react";
import { newsApi } from "../../../api/newsApi";
import { Button, Icon, Input } from "../../../ui";
import Container from "../../Container/Container";
import "./AddNewPost.scss";

export default function AddNewPost() {

    let user;
    localStorage.getItem("user") ? user = "admin" : user = "";
    const [addPost, setAddPost] = useState(false);

    const inputs = [
        {
            id: 1,
            label: "Title",
            placeholder: "Title",
            name: "title"
        },
        {
            id: 2,
            label: "Image link",
            placeholder: "src",
            name: "src"
        },
        {
            id: 3,
            label: "Subtitle",
            placeholder: "Subtitle",
            name: "subtitle"
        },
        {
            id: 4,
            label: "Text",
            placeholder: "Text",
            name: "text"
        },
    ];
    
    return (
        user ?
            <section className="add-post">
                <Container>
                    <div
                        className="add-post__wrap"
                        onMouseMove={() => setAddPost(true)}
                        onMouseLeave={() => setAddPost(false)}
                    >
                        <div className="add-post__favourites">
                            <p>add a post</p>
                            <Icon name={addPost ? "hide_window" : "add_post_actual"}/>
                        </div>
                        <div 
                            className="add-post__input_data"
                            style={{height: addPost ? "500px" : "0px"}}
                        >
                            <div className="add-post__content">
                                <Formik
                                    initialValues={{
                                        title: "",
                                        src: "",
                                        subtitle: "",
                                        text: "",
                                        radio: ""
                                    }}
                                    validateOnBlur
                                    onSubmit={(values) => 
                                        newsApi.createPost({
                                            ...values
                                        }).then(res => {
                                            console.log(res);
                                            window.location.reload();
                                        })
                                    }
                                >
                                    {({
                                        values, handleChange, handleBlur, handleSubmit, 
                                    }) => 
                                        <div>
                                            {inputs.map(({id, label, name, placeholder}) => 
                                                <div key={id}>
                                                    <Input 
                                                        label={label} 
                                                        placeholder={placeholder} 
                                                        margin="20px 0 0 0" 
                                                        name={name} 
                                                        width="295px"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.name}
                                                    />
                                                </div>
                                            )}
                                            <Button onClick={handleSubmit} margin="10px 0 0 0" text="Create"/>
                                        </div>
                                    }
                                </Formik>
                            </div>
                        </div>
                    </div>
                </Container>
            </section>
        : ""
    );
};