import { Fragment } from "react";
import { Routes, Route } from "react-router-dom";
import { Header, Home, InfoNews, NoteFoundPage } from ".";
import "./App.scss";

export default function App() {

  return (
    <Fragment>
      <Header>
        <Routes>
          <Route path="/" element={<Home />}/>
          <Route path="/posts/:id" element={<InfoNews />}/>
          <Route path="*" element={<NoteFoundPage />} exact/>
        </Routes>
      </Header>
    </Fragment>
  );
}