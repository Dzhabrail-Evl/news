import { Fragment } from "react";
import { useNavigate } from "react-router-dom";
import "./NoteFoundPage.scss";

export default function NoteFoundPage() {

    const navigate = useNavigate();
    const goBack = () => navigate('/');

    return (
        <div className="not-found">
            <div className="not-found__img"></div>
            <h1 onClick={goBack}>На главную</h1>
        </div>
    );
};