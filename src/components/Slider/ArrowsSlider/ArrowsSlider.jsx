import { useEffect } from "react";
import { Icon } from "../../../ui";
import "./ArrowsSlider.scss";

export default function ArrowsSlider({children, length, setSliderFlipping, sliderFlipping}) {

    const checkBoundaries = () => {

        const lastSlide = (length * 100) - 100;
    
        if (sliderFlipping > -100) {
            setSliderFlipping(0)
        } else if (-sliderFlipping > lastSlide) {
            setSliderFlipping(-lastSlide)
        };
    };

    useEffect(() => {
        checkBoundaries();
    }, [sliderFlipping]);

    const arrow_slides = [
        {
            id: 1,
            className: "arrow-slider__arrow-left",
            onClick: () => setSliderFlipping(sliderFlipping + 100),
            name: "arrow_left"
        },
        {
            id: 2,
            child: true
        },
        {
            id: 3,
            className: "arrow-slider__arrow-right",
            onClick: () => setSliderFlipping(sliderFlipping - 100),
            name: "arrow_right"
        },
    ];

    return (
        <div className="arrow-slider">
            {arrow_slides.map(({id, className, onClick, name, child}) => 
                <div key={id}>
                    <div className={className} onClick={onClick} style={{display: length <= 1 ? "none" : "flex"}}>
                        <Icon name={name} />
                    </div>
                    {child ? 
                        children
                    : ""}
                </div>
            )}
        </div>
    );
};