import ArrowsSlider from "./ArrowsSlider/ArrowsSlider";
import NavigationCircles from "./NavigationCircles/NavigationCircles";

export {ArrowsSlider, NavigationCircles};