import { useState } from "react";
import { Button, Icon, Slider } from "../../../ui";
import { newsApi } from "../../../api/newsApi";
import { Link } from "react-router-dom";
import "./CurrentNews.scss";

export default function CurrentNews({ actualNews }) {

    const [sliderFlipping, setSliderFlipping] = useState(0);
    let user;
    localStorage.getItem("user") ? user = "admin" : user = "";

    const addCurrentNews = (id) => {
        newsApi.changePostActual(id, false);
        window.location.reload();
    }

    return (
        <section className="news-main">
            <Slider length={actualNews?.length} setSliderFlipping={setSliderFlipping} sliderFlipping={sliderFlipping} width="800px" height="500px">
                <div className="news-main__items">
                    {actualNews?.map(post => 
                        <div 
                            key={post.id}
                            className="news-main__item"
                            style={{transform: `translateX(${sliderFlipping}%)`}}
                        >
                            {user ? 
                                <div className="news-main__favourites" onClick={() => addCurrentNews(post.id)}>
                                    <Icon name="delete_post_actual"/>
                                </div>
                            : ""}
                            <img className="news-main__news-logo" src={post.src} alt=""/>
                            <h2 className="news-main__title">{post.title}</h2>
                            <h3 className="news-main__subtitle">{post.subtitle}</h3>
                            <p className="news-main__description">{post.description}</p>
                            <p className="news-main__text">{post.text}</p>
                            <Link to={`/posts/${post.id}`}>
                                <Button text="Подробнее" margin="35px auto 20px auto"/>
                            </Link>
                        </div>
                    )}
                </div>
            </Slider>
        </section>
    );
};