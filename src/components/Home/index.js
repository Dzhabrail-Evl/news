import CurrentNews from "./CurrentNews/CurrentNews";
import AllNews from "./AllNews/AllNews";
import AddNewPost from "./AddNewPost/AddNewPost";

export {
    CurrentNews, AllNews, AddNewPost
}