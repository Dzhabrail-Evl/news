import { useEffect, useState } from "react";
import "./NavigationCircles.scss";

export default function NavigationCircles({length, sliderFlipping, setSliderFlipping}) {

    const [circles, setCircles] = useState([])
    
    useEffect(() => {
        setCircles(prev => {
            for(let i = 1; i <= length; i++) {
                prev[i] = i;
            }
            return prev
        })
    }, [length])
    let roundelLeftSide = (-sliderFlipping / 100 + 1);
    let roundelRightSide = (+sliderFlipping / 100 + 1);

    return (
        <div className="navigation-circles">
            {circles.map((i) => {
                return (
                    <div 
                        key={i} 
                        style={{display: length <= 1 ? "none" : "block"}}
                        className={`
                            navigation-circles__circle
                            ${roundelLeftSide === i || roundelRightSide === i ? "navigation-circles__circle--active" : ""}
                        `} 
                        onClick={() => setSliderFlipping(-(i - 1) * 100)}
                    />
                )
            })}
        </div>
    );
};