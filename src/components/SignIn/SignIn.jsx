import { useState } from "react";
import { authApi } from "../../api/authApi";
import { Button, Input } from "../../ui";
import "./SignIn.scss";

export default function SignIn({ setIsOpen }) {

    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [errorMessage, setErrorMessage] = useState();

    const inputs = [
        {
            id: 1,
            label: "email",
            placeholder: "test@gmail.com",
            type: "email",
            onChange: e => setEmail(e.target.value),
            check: email && email
        },
        {
            id: 2,
            margin: "15px 0 0 0",
            label: "password",
            placeholder: "password",
            type: "password",
            onChange: e => setPassword(e.target.value),
            check: password && password
        }
    ];

    const login = () => {
        authApi.login({
            email: email,
            password: password
        }).then(() => {
            setIsOpen(false);
            window.location.reload();
        }).catch(() => {
            setErrorMessage("Wrong login or password");

        });
    };

    // email: "admin@akhter.com",
    // password: "admin"

    return (
        <div className="sign-in">
            <h2 className="sign-in__title">Авторизация</h2>
            {inputs.map(({id,  margin, label, placeholder, type, onChange, check}) => 
                <Input 
                    key={id} 
                    margin={margin} 
                    label={label} 
                    placeholder={placeholder} 
                    type={type} 
                    required={true} 
                    onChange={onChange}
                    check={check}
                />
            )}
            <p className="sign-in__error-message">{errorMessage}</p>
            <Button 
                type="submit" 
                margin="35px 0 0 0" 
                text="Войти"
                onClick={login}
            />
        </div>
    );
};