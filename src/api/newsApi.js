import Api from ".";

const allNews = "664/all_news";

export const newsApi = {
    getAllNews: () => Api.get(`${allNews}`), //получить все новости
    getIdNews: (id) => Api.get(`${allNews}/${id}`), // получить данные по id 

    createPost: (params) => Api.post(`${allNews}`, {...params}), // добавить пост
    deletePost: (id) => Api.delete(`${allNews}/${id}`), // удалить пост

    changePostActual: (id, actual) => Api.patch(`${allNews}/${id}`, {actual}), // изменить поле внутри поста
    // changePostActual: (id) => Api.patch(`${allNews}/${id}`), // изменить поле внутри поста locale state
    
    // Для изменения полей в постах
    changePostImg: (id, src) => Api.patch(`${allNews}/${id}`, {src}), // изменить поле внутри поста
    changePostTitle: (id, title) => Api.patch(`${allNews}/${id}`, {title}), // изменить поле внутри поста
    changePostSubtitle: (id, subtitle) => Api.patch(`${allNews}/${id}`, {subtitle}), // изменить поле внутри поста
    changePostText: (id, text) => Api.patch(`${allNews}/${id}`, {text}), // изменить поле внутри поста
}