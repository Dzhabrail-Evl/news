import { Fragment, useEffect, useState, createContext } from "react";
import { newsApi } from "../../api/newsApi";
import { AllNews, CurrentNews, AddNewPost } from "../../components/Home";
export const NewsContext = createContext();

export default function Home() {
    
    const [allNews, setAllNews] = useState();
    const actualNews = allNews?.filter(word => word.actual);

    useEffect(() => {
        newsApi.getAllNews().then(res => {
            setAllNews(res);
        });
    },[]);

    return (
        <Fragment>
            <NewsContext.Provider value={setAllNews}>
                <CurrentNews actualNews={actualNews} setAllNews={setAllNews}/>
                <AddNewPost />
                <AllNews allNews={allNews}/>
            </NewsContext.Provider>
        </Fragment>
    );
};