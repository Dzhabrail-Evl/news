import { useState } from "react";
import SignIn from "../SignIn/SignIn";
import { Icon, Modal } from "../../ui";
import "./Auth.scss";

export default function Auth() {

    const [isOpen, setIsOpen] = useState(false);
    let user;
    localStorage.getItem("user") ? user = "admin" : user = "";

    const atorizationСheck = () => {
        if(user) {
            setIsOpen(false)
            localStorage.removeItem("user")
            localStorage.removeItem("token")
            window.location.reload();
        } else {
            setIsOpen(true)
        }
    }

    return (
        <section className="auth">
            <div className="auth__name">
                <h3>{user}</h3>
            </div>
            <div className="auth__icon" onClick={() => atorizationСheck()}>
                <Icon name={user ? "log_out" : "log_in"}/>
            </div>
            {isOpen ?
                <Modal setIsOpen={setIsOpen} isOpen={isOpen}>
                    <SignIn setIsOpen={setIsOpen}/>
                </Modal>
            : ""}
        </section>
    );
};